package gitea

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

func (g *Gitea) WebhookDeploy(user, repo, keypriv, keypub, cloneurl, multiweb string) error {
	type WebhookData struct {
		USER        string
		REPO        string
		PUB         string
		KEY         string
		SECRET      string
		CLONESSHURL string
		MULTIWEB    string
	}
	keypriv = strings.Replace(keypriv, "\n", "|", -1) // key to oneline string, no breaks, '|' (pipe) is newline.
	whData := WebhookData{user, repo, keypub, keypriv, g.webhookSecret, cloneurl, multiweb }
	jsonData, err := json.Marshal(whData)
	if err != nil && err.Error() != "<nil>" {
		log.Printf("Error marshaling data for webhook.")
		return err
	}

	req, err := http.NewRequest("POST", g.webhookURL, bytes.NewBuffer(jsonData))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		// webhook listener must response 400 if trigger rule (webhooksecret) is not satisfied
		if resp.StatusCode == 400 {
			return fmt.Errorf("Auth fail: %d", resp.StatusCode)
		} else {
			return fmt.Errorf("http code error: %d", resp.StatusCode)
		}
	}
	defer resp.Body.Close()
	return nil
}
