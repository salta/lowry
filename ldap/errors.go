package ldap

import (
	"errors"
)

var ErrAlreadyExist = errors.New("Already exist an entry with this uid/cn")
var ErrNotFound = errors.New("Can't find the record in the ldap")
