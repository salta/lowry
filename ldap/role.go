package ldap

import (
	"log"
	"strings"
)

type Role int

const (
	Undefined Role = iota
	Amiga
	Sindominante
)

func RoleFromString(s string) Role {
	switch strings.ToLower(s) {
	case "amiga":
		return Amiga
	case "sindominante":
		return Sindominante
	default:
		log.Printf("Not valid role: %s", s)
		return Undefined
	}
}

func (r Role) String() string {
	switch r {
	case Amiga:
		return "amiga"
	case Sindominante:
		return "sindominante"
	}
	return ""
}
